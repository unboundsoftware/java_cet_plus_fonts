FROM openjdk:11.0.16@sha256:99bac5bf83633e3c7399aed725c8415e7b569b54e03e4599e580fc9cdb7c21ab
MAINTAINER Joakim Olsson <joakim@unbound.se>

RUN apt-get update \
    && apt-get install -y curl wget cabextract xfonts-utils libfontenc1 xfonts-encodings fonts-droid-fallback \
    && rm -r /var/lib/apt/lists/*

RUN curl -SL http://ftp.se.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.8.1_all.deb \
    -o /tmp/msttfont.deb \
    && dpkg --install /tmp/msttfont.deb \
    && rm /tmp/msttfont.deb

RUN echo "Europe/Stockholm" > /etc/timezone; dpkg-reconfigure -f noninteractive tzdata
